.mode csv
.import stores.csv temp

update temp SET Location = Replace(Location, '}''','}');
update temp SET Location = Replace(Location, '''{', '{');
update temp SET Location = Replace(Location, '''', '"');
update temp SET Location = Replace(Location, 'False', 'false');

insert into
stores 
(
	"county", 
	"license_number", 
	"operation_type", 
	"establishment_type", 
	"entity_name", 
	"dba_name", 
	"street_number", 
	"street_name", 
	"address_line_2", 
	"address_line_3", 
	"city" , 
	"state", 
	"zip_code", 
	"square_footage", 
	"longitude", 
	"latitude" ,
	"needs_recoding",
	"human_address_address", 
	"human_address_state" , 
	"human_address_zip" 
)
select 
"County",
"License Number",
"Operation Type",
"Establishment Type",
"Entity Name",
"DBA Name",
"Street Number",
"Street Name",
"Address Line 2",
"Address Line 3",
"City",
"State",
"Zip Code",
"Square Footage",
ifnull(json_extract(Location, '$.longitude'), 0) ,
ifnull(json_extract(Location, '$.latitude'), 0) ,
json_extract(Location, '$.needs_recoding'),
json_extract(Location, '$.human_address.city') ,
json_extract(Location, '$.human_address.state') ,
json_extract(Location, '$.human_address.zip') 
from temp where Location <> '' ;