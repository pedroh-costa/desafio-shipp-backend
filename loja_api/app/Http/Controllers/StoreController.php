<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class StoreController extends Controller
{
     
    public function __construct()
    {
        //
    }
	
	public function index()
    {
		$latitude = Input::get('latitude');
        $longitude = Input::get('longitude');
		
		$rad	= 6.5;
		$R 		= 6371;  
		$maxLat = $latitude + rad2deg($rad/$R);
		$minLat = $latitude - rad2deg($rad/$R);
		$maxLon = $longitude + rad2deg(asin($rad/$R) / cos(deg2rad($latitude)));
		$minLon = $longitude - rad2deg(asin($rad/$R) / cos(deg2rad($latitude)));
		
		$query	= "";
		$query	.= " SELECT * FROM stores					";
		$query	.= " WHERE 									";
		$query	.= " latitude BETWEEN $minLat AND $maxLat 	";
		$query	.= " AND								 	";
		$query	.= " longitude BETWEEN $minLon AND $maxLon 	";
		//$query	.= " LIMIT 100							 	";
		
		$results = DB::select($query);	
		$data = [];	
        foreach($results as $row) {
			$d = distancia($latitude, $longitude, $row->latitude, $row->longitude);
			$data[] = [
				"county"				 	=> $row->county					,
				"license_number"			=> $row->license_number         ,
				"operation_type"			=> $row->operation_type         ,
				"establishment_type"		=> $row->establishment_type     ,
				"entity_name"				=> $row->entity_name            ,
				"dba_name"					=> $row->dba_name               ,
				"street_number"				=> $row->street_number          ,
				"street_name"				=> $row->street_name            ,
				"address_line_2"			=> $row->address_line_2         ,
				"address_line_3"			=> $row->address_line_3         ,
				"city" 						=> $row->city                   ,
				"state"						=> $row->state                  ,
				"zip_code"					=> $row->zip_code               ,
				"square_footage"			=> $row->square_footage         ,
				"longitude"					=> $row->longitude              ,
				"latitude" 					=> $row->latitude               ,
				"needs_recoding"			=> $row->needs_recoding         ,
				"human_address_address"		=> $row->human_address_address  ,
				"human_address_state" 		=> $row->human_address_state    ,
				"human_address_zip" 		=> $row->human_address_zip      ,
				'distance'					=> $d			
			];	
		}	
		
		foreach ($data as $key => $row)
		{
			$count[$key] = $row['distance'];
		}
		array_multisort($count, SORT_ASC, $data);
		return response()->json($data, 200);
		
    }
	
}
