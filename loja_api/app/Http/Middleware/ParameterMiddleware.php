<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Input;

use Closure;

class ParameterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (!$request->has('latitude') || !$request->has('longitude')) {
            return response()->json(['message' => 'Bad Request!'], 400 );
        }
        return $next($request);
    }
}
