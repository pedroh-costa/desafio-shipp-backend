<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Input;
use App\Logs;
use Closure;

class LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$response = $next($request);
		
		$latitude = Input::get('latitude');
        $longitude = Input::get('longitude');
		
		$number_store = count(json_decode($response->getContent(), true));
		
		Logs::create([
			'latitude' => $latitude,
			'longitude' => $longitude,
			'status_code' => $response->getStatusCode(),
			'number_store' => $number_store,
		]);

        return $response;
    }
}
