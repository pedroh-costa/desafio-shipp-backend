
# Import

### import dados csv

0) Acessar sqlite, executar comando abaixo
´´´
sqlite3 database/database.sqlite
´´´

1) executar comando abaixo
´´´
.mode csv
´´´

2) executar comando abaixo
´´´
.import stores.csv temp
´´´

3) executar comando abaixo
´´´
update temp SET Location = Replace(Location, '}''','}');
update temp SET Location = Replace(Location, '''{', '{');
update temp SET Location = Replace(Location, '''', '"');
update temp SET Location = Replace(Location, 'False', 'false');

´´´


4) executar comando abaixo
´´´

insert into
stores 
(
	"county", 
	"license_number", 
	"operation_type", 
	"establishment_type", 
	"entity_name", 
	"dba_name", 
	"street_number", 
	"street_name", 
	"address_line_2", 
	"address_line_3", 
	"city" , 
	"state", 
	"zip_code", 
	"square_footage", 
	"longitude", 
	"latitude" ,
	"needs_recoding",
	"human_address_address", 
	"human_address_state" , 
	"human_address_zip" 
)
select 
"County",
"License Number",
"Operation Type",
"Establishment Type",
"Entity Name",
"DBA Name",
"Street Number",
"Street Name",
"Address Line 2",
"Address Line 3",
"City",
"State",
"Zip Code",
"Square Footage",
ifnull(json_extract(Location, '$.longitude'), 0) ,
ifnull(json_extract(Location, '$.latitude'), 0) ,
json_extract(Location, '$.needs_recoding'),
'' ,
'' ,
'' 
from temp where Location <> '' ;

´´´

### subir servidor

1) Acessar pasta da aplicação e executar comando abaixo:
´´´
 php -S 127.0.0.1:8000 -t public
´´´

### rodar tests

1) Acessar pasta da aplicação e executar comando abaixo:
´´´
  vendor/bin/phpunit
´´´
