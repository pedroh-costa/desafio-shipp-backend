<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Logs;

class StoreTest extends TestCase
{
	Use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testParameterInvalid()
    {
        $this->get('/v1/stores?longitude=-73.700539&latitud=42.754424');
		$this->assertEquals(400, $this->response->status());
    }
	
	public function testParameterValid()
    {
        $this->get('/v1/stores?longitude=-73.700539&latitude=42.754424');
		$this->assertEquals(200, $this->response->status());
    }
	
	public function testCreateLog()
    {
        Logs::create([
			'latitude' => -73.700539,
			'longitude' => 42.754424,
			'status_code' =>  200,
			'number_store' => 10,
		]);
		$this->seeInDataBase('logs', ['status_code'=>200, 'number_store'=>10]);
	}
	
	public function testResultStore()
    {
		$this->get('/v1/stores?longitude=-73.700539&latitude=42.754424');
		$this->assertEquals(200, $this->response->status());
		$this->seeJsonStructure([
				'*' => [
					'county',
					'license_number',
					'longitude',
					'latitude',
					'distance',
				]
		]);
	}
	
	
}
