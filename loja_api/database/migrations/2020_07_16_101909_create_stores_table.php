<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string("county"						);
			$table->string("license_number"     		);
			$table->string("operation_type"     		);
			$table->string("establishment_type" 		);
			$table->string("entity_name"        		);
			$table->string("dba_name"           		);
			$table->string("street_number"      		);
			$table->string("street_name"        		);
			$table->string("address_line_2"     		);
			$table->string("address_line_3"     		);
			$table->string("city"               		);
			$table->string("state"              		);
			$table->string("zip_code"           		);
			$table->string("square_footage"				);
			$table->decimal("longitude"					,10,6);
			$table->boolean("needs_recoding"			);
			$table->string("human_address_address"		);
			$table->string("human_address_state"		);
			$table->string("human_address_zip"			);
			$table->decimal("latitude"					,10,6);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
