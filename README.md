# Importar dados/Subir servidor/Rodar os testes


### Importar dados do csv

1) Acessar pasta da aplicação

```sh
cd loja_api
```

2) Copiar o arquivo stores.csv para pasta da aplicação

3) Acessar sqlite, executar comando abaixo
```sh
sqlite3 database/database.sqlite
```

4) Executar o comando abaixo
```sh
.mode csv
```

5) Executar o comando abaixo
```sh
.import stores.csv temp
```

6) Executar o comando abaixo
```sh
update temp SET Location = Replace(Location, '}''','}');
update temp SET Location = Replace(Location, '''{', '{');
update temp SET Location = Replace(Location, '''', '"');
update temp SET Location = Replace(Location, 'False', 'false');
```

7) Executar o comando abaixo
```sh

insert into
stores 
(
	"county", 
	"license_number", 
	"operation_type", 
	"establishment_type", 
	"entity_name", 
	"dba_name", 
	"street_number", 
	"street_name", 
	"address_line_2", 
	"address_line_3", 
	"city" , 
	"state", 
	"zip_code", 
	"square_footage", 
	"longitude", 
	"latitude" ,
	"needs_recoding",
	"human_address_address", 
	"human_address_state" , 
	"human_address_zip" 
)
select 
"County",
"License Number",
"Operation Type",
"Establishment Type",
"Entity Name",
"DBA Name",
"Street Number",
"Street Name",
"Address Line 2",
"Address Line 3",
"City",
"State",
"Zip Code",
"Square Footage",
ifnull(json_extract(Location, '$.longitude'), 0) ,
ifnull(json_extract(Location, '$.latitude'), 0) ,
json_extract(Location, '$.needs_recoding'),
json_extract(Location, '$.human_address.city') ,
json_extract(Location, '$.human_address.state') ,
json_extract(Location, '$.human_address.zip') 
from temp where Location <> '' ;
```


### Instalação 

1) Instalação
```sh
 composer install
```
2) Criação tabelas (executar o comando apenas se for criar um banco de dados novo)
```sh
php artisan migrate
```


### Subir servidor

1) Acessar pasta da aplicação e executar comando abaixo:
```sh
 php -S 127.0.0.1:8000 -t public
```

### Rodar tests

1) Acessar pasta da aplicação e executar comando abaixo:
```sh
  vendor/bin/phpunit
```

### Testar a API

1) Acessar o navagegador ou qualquer outra aplicação de teste de API ex.: postman

http://localhost:8000/v1/stores?longitude=-73.699799&latitude=42.76352